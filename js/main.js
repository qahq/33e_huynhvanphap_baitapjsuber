const UBER_CAR="uberCar";
const UBER_SUV="uberSUV";
const UBER_BLACK="uberBlack";

function tinhGiaTienKmDauTien(car){
    if (car == UBER_CAR){
        return 8000;
    }
    if (car == UBER_SUV){
        return 9000;
    }
    if (car == UBER_BLACK){
        return 10000;
    }
}
function tinhGiaTienKm1_19(car){
    switch (car){
        case UBER_CAR:{
            return 7500;
        }
        case UBER_SUV:{
            return 8500;
        }
        case UBER_BLACK:{
            return 9500;
        }
        default:
            return 0;
    }

}
function tinhGiaTienKm19TroDi(car){
    switch (car){
        case UBER_CAR: {
            return 7000;
        }
        case UBER_SUV: {
            return 8000;
        }
        case UBER_BLACK: {
            return 9000;
        }
        default:
            return 0;
    }

}

// main function
function tinhTienUber(){
    var carOption = document.querySelector('input[name="selector"]:checked').value;
    console.log('carOption: ', carOption);


    var giaTienKmDauTien = tinhGiaTienKmDauTien(carOption);
    console.log('giaTienKmDauTien: ', giaTienKmDauTien);

    var giaTienKm1_19 = tinhGiaTienKm1_19 (carOption);
    console.log('giaTienKm1_19: ', giaTienKm1_19);

    var giaTienKm19TroDi =tinhGiaTienKm19TroDi(carOption);
    console.log('giaTienKm19TroDi: ', giaTienKm19TroDi);


    var soKmDi = document.getElementById("txt-km").value * 1;
    var tienPhaiTra = 0;
    if (soKmDi <= 1) {
    tienPhaiTra = giaTienKmDauTien * soKmDi;
    } else if (soKmDi > 1 && soKmDi <= 19) {
    tienPhaiTra = giaTienKmDauTien + (soKmDi - 1) * giaTienKm1_19;
    } else {
    tienPhaiTra = giaTienKmDauTien+ 18 * giaTienKm1_19 + (soKmDi - 19) * giaTienKm19TroDi;
    }
  console.log("Tiền phải trả: ", tienPhaiTra);
  document.getElementById("divThanhTien").style.display = "block";
  document.getElementById("divThanhTien").innerHTML = `<h2>Tiền phải thanh toán: ${tienPhaiTra.toLocaleString()} VND</h2>`;
}



